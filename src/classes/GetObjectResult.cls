global class GetObjectResult
{
	webservice integer					NumResults;
	webservice boolean					Error;
	webservice string					ErrorMessage;
	webservice list<WSAccountObject>	Objects;
	
	global GetObjectResult()
	{
		NumResults = 0;
		Error = false;
		ErrorMessage = '';
		Objects = new list<WSAccountObject>();
	}
}