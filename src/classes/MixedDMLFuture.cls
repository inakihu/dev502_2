public class MixedDMLFuture {
    public static void useFutureMethod() {
        // First DML operation
        Account a = new Account(Name='Acme');
        insert a;
        
        // This next operation (insert a user with a role) 
        // can't be mixed with the previous insert unless 
        // it is within a future method. 
        // Call future method to insert a user with a role.
        Util.insertUserWithRole(
            'mruiz@awcomputing.com', 'mruiz', 
            'mruiz@awcomputing.com', 'Ruiz');        
    }
    
    public static void method() {
        // First DML operation
        Account a = new Account(Name='Acme');
        insert a;
        
        // This next operation (insert a user with a role) 
        // can't be mixed with the previous insert unless 
        // it is within a future method. 
        // Call future method to insert a user with a role.
        Profile p = [SELECT Id FROM Profile LIMIT 1];
        UserRole r = [SELECT Id FROM UserRole WHERE Name='COO'];
        // Create new user with a non-null user role ID 
        User u = new User(alias = 'mruiz', email='mruiz@awcomputing.com', 
            emailencodingkey='UTF-8', lastname='Ruiz', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
            timezonesidkey='America/Los_Angeles', 
            username='mruiz@awcomputing.com');
        insert u;        
    }
    
    public static void methodUpdate() {
        // First DML operation
        Account a = new Account(Name='Acme 22');
        insert a; 
        
        User u = [SELECT Id,LastName, FirstName, UserRoleId FROM User WHERE Id = :UserInfo.getUserId()];
        UserRole r = [SELECT Id FROM UserRole WHERE Id!=:u.UserRoleId Limit 1];
        u.Department = 'Departament 001';
        u.userroleid = r.Id; 
        update u;    
        
        
        
        
    }
}