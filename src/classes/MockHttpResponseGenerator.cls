/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "results" : [ { "address_components" : [ { "long_name" : "213", "short_name" : "213", "types" : [ "street_number" ] }, { "long_name" : "Paseo de la Castellana", "short_name" : "Paseo de la Castellana", "types" : [ "route" ] }, { "long_name" : "Madrid", "short_name" : "Madrid", "types" : [ "locality", "political" ] }, { "long_name" : "Madrid", "short_name" : "M", "types" : [ "administrative_area_level_2", "political" ] }, { "long_name" : "Comunidad de Madrid", "short_name" : "Comunidad de Madrid", "types" : [ "administrative_area_level_1", "political" ] }, { "long_name" : "España", "short_name" : "ES", "types" : [ "country", "political" ] }, { "long_name" : "28029", "short_name" : "28029", "types" : [ "postal_code" ] } ], "formatted_address" : "Paseo de la Castellana, 213, 28029 Madrid, Madrid, España", "geometry" : { "location" : { "lat" : 40.4704731, "lng" : -3.6892066 }, "location_type" : "ROOFTOP", "viewport" : { "northeast" : { "lat" : 40.4718220802915, "lng" : -3.687857619708498 }, "southwest" : { "lat" : 40.4691241197085, "lng" : -3.690555580291502 } } }, "partial_match" : true, "place_id" : "ChIJ5ay4wBMpQg0RA_eNTTxXFXQ", "types" : [ "street_address" ] } ], "status" : "OK" }');
        res.setStatusCode(200);
        return res;
    }
}