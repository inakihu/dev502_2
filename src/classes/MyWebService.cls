global class MyWebService {
    webService static Id makeContact(String lastName, Account a) { 
		Contact c = new Contact(lastName = 'Weissman', AccountId = a.Id); 
		insert c; 
		return c.id;
	}
	
	webService static Id makeContactToAccountId(String lastName, String AccountId) { 
		Contact c = new Contact(lastName = 'Weissman', AccountId = AccountId); 
		insert c; 
		return c.id;
	}
	
	webService static GetObjectResult QueryAccountWithContacts() { 
		List<Object> lstObjectsc = [SELECT id,Name,(SELECT Id, FirstName,LastName from Contacts) FROM Account];
		GetObjectResult res = new GetObjectResult();
	
	
		/*if(req == null)
		{
			res.ErrorMessage = 'Invalid request.';
			res.Error = true;
			return res;
		}
	
		if(req.ObjectName == null)
		{
			res.ErrorMessage = 'ObjectName can not be null.';
			res.Error = true;
			return res;
		}*/
	
		for(Account sObjAcc : [SELECT id,Name,(SELECT Id, FirstName,LastName,Phone from Contacts) FROM Account])
			{
			WSAccountObject objAcc = new WSAccountObject();
			objAcc.IdSF = sObjAcc.Id;
			objAcc.Name = sObjAcc.Name;
			List<Contact> lstContacts = sObjAcc.Contacts;
			for(Contact sObjCon : lstContacts){
				WSContactObject objContact = new WSContactObject();
				objContact.IdSF = sObjCon.Id;
				objContact.FirstName = sObjCon.FirstName;
				objContact.LastName = sObjCon.LastName; 
				objContact.Phone = sObjCon.Phone;
				objAcc.lstContacts.add(objContact);	
			}
			res.Objects.add(objAcc);
		}

		res.NumResults = res.Objects.size();
		return res;
	} 
    
}