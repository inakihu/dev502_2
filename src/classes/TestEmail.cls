@isTest
private class TestEmail {
	@isTest static void TestEmail() {
		Contact c = new Contact();
		c.LastName = 'laastname';
		c.email = 'aaaa@gmail.com';
		insert c;
		
        // Create a new email and envelope object
        Messaging.InboundEmail email  = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.plainTextBody = 'This should become a note';
        email.fromAddress = 'aaaa@gmail.com';
        
        TestEmailService edr = new TestEmailService();
        
        Test.startTest();
        Messaging.InboundEmailResult result = edr.handleInboundEmail(email, env);
        Test.stopTest();
        
    } 
}
