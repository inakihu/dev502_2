/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class TestEmailService implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        System.debug('Start TestEmailService');
        System.debug('Envelope: ' + envelope);
        System.debug('Mail: ' + email);
        List<Contact> lstC = [SELECT Id,Name FROM Contact WHERE email= :email.fromAddress];
        if (lstC.size()>0){
        	Task t = new Task();
        	t.WhoId = lstC[0].Id;
        	t.Subject= 'Usuario a enviado un email.';
        	t.ActivityDate = Date.today();
        	insert t;
        }
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = false;
        return result;
    }
}