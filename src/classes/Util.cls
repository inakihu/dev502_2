public class Util {
    @future
    public static void insertUserWithRole(
        String uname, String al, String em, String lname) {

        Profile p = [SELECT Id FROM Profile LIMIT 1];
        UserRole r = [SELECT Id FROM UserRole WHERE Name='COO'];
        // Create new user with a non-null user role ID 
        User u = new User(alias = al, email=em, 
            emailencodingkey='UTF-8', lastname=lname, 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
            timezonesidkey='America/Los_Angeles', 
            username=uname);
        insert u;
    }
    
    @future(callout=true)
    public static void updateAccountsLocation(List<ID> cuentas) {
    	System.debug('IDs: ' + cuentas);
        Account[] accs = [SELECT Id,ShippingCity,ShippingCountry,ShippingLatitude,ShippingLongitude,ShippingPostalCode,ShippingState,ShippingStreet,Location__c,Location__Latitude__s,Location__Longitude__s FROM Account WHERE Id IN :cuentas];
        for(Integer i=0;i<accs.size();i++){
        	String address = accs[i].ShippingStreet + ' ' + accs[i].ShippingCity + ' ' + accs[i].ShippingPostalCode + ' ' + accs[i].ShippingCountry + ' ';
        	HTTPResponse res = CalloutClass.getLocationFromGoogle(address);
        	UtilUpdate.updateAccountFromGoogleResponse(accs[i],res); 
	    }
	    update accs;
    }
    
    
    @future(callout=true)
    public static void updateAccountLocation(String idAccount) {
        Account acc = [SELECT Id,ShippingAddress,ShippingCity,ShippingCountry,ShippingLatitude,ShippingLongitude,ShippingPostalCode,ShippingState,ShippingStreet,Location__c,Location__Latitude__s,Location__Longitude__s FROM Account WHERE Id = :idAccount];
        HttpRequest req = new HttpRequest();
     	req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?Address='  + acc.ShippingStreet + ' ' + acc.ShippingCity + ' ' + acc.ShippingPostalCode + ' ' + acc.ShippingCountry + ' ');
		req.setMethod('GET');
		Http http = new Http();
        HTTPResponse res = http.send(req);
		System.debug(res.getBody());
    }
}