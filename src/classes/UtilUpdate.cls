public class UtilUpdate {
	public static void updateAccountFromGoogleResponse(Account a,HTTPResponse res){
    	Map<String, Object> responseJSON = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        	//System.debug('responseJSON:' + responseJSON);
        	List<Object> lst = (List<Object>)responseJSON.get('results');
        	//System.debug('lst[0]:' + lst[0]);
        	Map<String, Object> objJSON = (Map<String, Object>)lst[0];
        	//System.debug('objJSON:' + objJSON);
        	Map<String, Object> geometryJSON = (Map<String, Object>)objJSON.get('geometry');
        	//System.debug('geometryJSON:' + geometryJSON);
        	Map<String, Object> locationJSON = (Map<String, Object>)geometryJSON.get('location');
        	//System.debug('locationJSON:' + locationJSON);
        	 
			a.Location__Latitude__s = (Decimal) locationJSON.get('lat');
			a.Location__Longitude__s = (Decimal)locationJSON.get('lng');
    }   
}