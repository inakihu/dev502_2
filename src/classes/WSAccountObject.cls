global class WSAccountObject {
	webservice string IdSF;
	webservice string Name;
	webservice List<WSContactObject> lstContacts;
	
	global WSAccountObject()
	{
		IdSF = '';
		Name = '';
		lstContacts = new list<WSContactObject>();
	}
}