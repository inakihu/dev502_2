global class bacheableJob implements Database.Batchable<sObject>{
//To use a callout in batch Apex, specify Database.AllowsCallouts in the class definition.
//global class bacheableJob implements Database.Batchable<sObject>,Database.AllowsCallouts {
 
 	global Database.QueryLocator start(Database.BatchableContext bc){
 		System.debug('Hola Bacheable Job');
		return Database.getQueryLocator([SELECT id, Account.Id, Account.Type FROM Opportunity WHERE StageName ='Closed Won']);
	} 
	
	global void execute(Database.BatchableContext bc, list<Opportunity> opps){
		System.debug('Vamos a actualizar: '+ opps.size());
		if(opps.size()>0){
			List<Account> lstAcc = new List<Account>();
			Set<ID> setIds = new Set<ID>();
			for(Integer i=0;i<opps.size();i++){
				opps[i].Account.Type ='HOLA';
				if(!setIds.contains(opps[i].AccountId)){
					setIds.add(opps[i].AccountId);
					lstAcc.add(opps[i].Account);
				}
			}
			System.debug('Actualizando: '+ lstAcc);
			update lstAcc; 
		}
	}
	
	global void finish(Database.BatchableContext bc){
		System.debug('Finalizado');
	}	
}