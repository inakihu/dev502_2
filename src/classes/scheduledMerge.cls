global class scheduledMerge implements Schedulable { 
	global void execute(SchedulableContext SC){ 
		Opportunity[] lstOpp = [SELECT id, Account.Id, Account.Type FROM Opportunity WHERE StageName ='Close Won'];
		if(lstOpp.size()>0){
			List<Account> lstAcc = new List<Account>(); 
			for(Integer i=0;i<lstOpp.size();i++){
				lstOpp[i].Account.Type ='HOLA';
				lstAcc.add(lstOpp[i].Account);
			}
			update lstAcc; 
		} 
	}
}