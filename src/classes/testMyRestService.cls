@isTest
private class testMyRestService {

	static {
		// setup test data  
	}

	@isTest static void testGetContact() {
		Contact c = new Contact();
		c.LastName = 'LastName';
		insert c;
		
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/v1/';  
	    req.httpMethod = 'GET';
	    
	    req.addParameter('Id', c.Id);
        
        RestContext.request = req;
        RestContext.response = res;
        
        MyRESTService.returnClassContact results = MyRESTService.getContact();
	}
  	
  	@isTest static void testGetCases() {
  		
  		Account a = new Account();
  		a.Name = 'AccountName';
  		insert a;
  		
  		List <Case> lstCases = new List <Case>(); 
  		for(Integer i=0;i<3;i++){
  			Case ca = new Case();
  			ca.Subject = 'Case' + 1;
  			ca.status = 'Status';
  			ca.Origin = 'Web';
  			lstCases.add(ca);
  		}
  		insert lstCases;
  		
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();
	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/v1/';  
	    req.httpMethod = 'POST';
		
		req.addParameter('companyName', a.Name);
		
		RestContext.request = req;
        RestContext.response = res;
            
	    MyRESTService.returnClassCases results = MyRESTService.getOpenCases();
	}

}