@isTest
private class testMyWebService
{
  	@isTest static void testMyWebService()
  	{
  		Account acc = new Account();
  		acc.Name = 'New Account';
  		insert acc;
		
		MyWebService.makeContact('LastName', acc);
		
		MyWebService.makeContactToAccountId('LastName', acc.Id);
      
      	MyWebService.QueryAccountWithContacts();
	}
}