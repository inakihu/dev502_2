trigger triggerAccount on Account (after update) {
    List<Account> listNewRecords = Trigger.new;
    List<Account> listOldRecords = Trigger.old;
    
    Set<ID> accountActiveChanged = new Set<ID>();
    for(Integer i=0;i<listNewRecords.size();i++){
        if(listOldRecords[i].ShippingStreet != listNewRecords[i].ShippingStreet){
            accountActiveChanged.add(listNewRecords[i].Id);
        }
    }
    System.debug('Changed: ' + accountActiveChanged);
    if(accountActiveChanged.size()>0){
    	List<ID> l=new List<ID>();
		l.addAll(accountActiveChanged);
		ID[] lAccs = l;
		System.debug('Call updateAccountsLocation');
        Util.updateAccountsLocation(lAccs);
    }
}